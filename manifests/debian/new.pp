# Releases jessie and newer

class apache::debian::new(
  $disable_TLS10RC4 = false,
  $disable_TLS11RC4 = false,
  $disable_3DES     = false,
){
  # Note: the readlink executable comes from the coreutils package.
  exec { 'a2dissite 000-default.conf':
    command => 'a2dissite 000-default.conf',
    onlyif  => 'readlink /etc/apache2/sites-enabled/000-default.conf',
    path    => '/bin:/usr/bin:/usr/sbin',
    require => Package['apache'],
  }

  # Disable weak SSL ciphers.
  apache::conf { 'ssl-strength.conf':
    ensure  => present,
    content => template('apache/etc/apache2/conf.d/ssl-strength.erb'),
  }

  # Replace the default Apache security configuration file with one that
  # suppresses most information disclosure about the server.
  apache::conf { 'security.conf':
    ensure => present,
    source => 'puppet:///modules/apache/etc/apache2/conf.d/security',
    require => Package['apache'],
    notify  => Service['apache'],
  }

  # Support /server-status for all virtual hosts, but only from localhost.
  apache::module { 'status':
    ensure  => present,
    require => File['/etc/apache2/mods-available/status.conf'],
  }
  file { '/etc/apache2/mods-available/status.conf':
    source  => 'puppet:///modules/apache/etc/apache2/mods-available/status.conf',
    require => Package['apache'],
    notify  => Service['apache'],
  }
}
