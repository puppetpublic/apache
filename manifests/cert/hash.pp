# Creates the OpenSSL hash symlinks for certificates.  Factored out for ensure
# handling and since it's used for both the main certificate and for the
# Comodo root certificate.

define apache::cert::hash(
  $ensure    = 'present',
  $directory = '/etc/ssl/certs'
) {
  if !($ensure in [ 'present', 'absent' ]) {
    fail("ensure must be present or absent, not $ensure")
  }
  $hashcommand = "`openssl x509 -noout -hash -in ${directory}/${name}`"

  # Create the link if ensure is present.
  if ($ensure == 'present') {
    exec { "openssl hash link ${directory}/${name}":
      path    => '/usr/bin',
      command => "ln -s ${directory}/${name} ${directory}/${hashcommand}.0",
      unless  => "[ -f \"${directory}/${hashcommand}.0\" ]",
      require => $::osfamily ? {
        'Debian' => [ File["${directory}/${name}"],
                      Package['ca-certificates'],
                      Package['openssl'] ],
        'RedHat' => [ File["${directory}/${name}"], Package['openssl'] ],
      },
    }
  }
}
