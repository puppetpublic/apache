# Apache configuration for Debian.
#
# This manifest cleans up some of the default Apache configuration for Debian
# that we don't want and installs some standard configuration we want to
# always have available for all web servers.
#
# Jessie is a significant change to the way that apache servers are
# configured.  Create a completely new class to support it to make the
# transition clearer.
#
# $disable_TLS10RC4 disables TLSv1.0 protocol AND the RC4-based cipher suites.
#
# $disable_3DES: if set to true will disable all DES- and 3DES-based
#   ciphers. See also https://www.openssl.org/blog/blog/2016/08/24/sweet32/

class apache::debian (
  $disable_TLS10RC4 = false,
  $disable_TLS11RC4 = false,
  $disable_3DES     = false,
){
  # Almost every Apache server uses SSL and even if a server does not use it, it
  # does no harm to enable it, so we enable mod_ssl for everyone.
  apache::module { "ssl": ensure => present }

  if ($lsbdistcodename == 'wheezy') {
    class { 'apache::debian::old':
      disable_TLS10RC4 => $disable_TLS10RC4,
      disable_TLS11RC4 => $disable_TLS11RC4,
      disable_3DES     => $disable_3DES,
    }
  } else {
    class { 'apache::debian::new':
      disable_TLS10RC4 => $disable_TLS10RC4,
      disable_TLS11RC4 => $disable_TLS11RC4,
      disable_3DES     => $disable_3DES,
    }
  }
}
