# Enable an Apache module.
#
# This uses the a2enmod tool provided by Debian to enable a module with its
# configuration.  Example:
#
#     apache::module { "rewrite": ensure => present }
#
# This infrastructure, and therefore this define, is only available on Debian.

define apache::module($ensure) {
  if ($::operatingsystem != 'debian') and ($::operatingsystem != 'ubuntu') {
    fail("Unsupported apache::module operating system ${::operatingsystem}")
  }
  if !($ensure in [ 'present', 'absent' ]) {
    fail("ensure must be present or absent, not $ensure")
  }

  # Set various local variables to keep the exec readable.
  $modsdir = '/etc/apache2/mods-enabled'
  $module  = "${modsdir}/${name}"
  $cmd = $ensure ? {
    present => "a2enmod ${name}",
    absent  => "a2dismod ${name}",
  }
  $condition = $ensure ? {
    present => "-e ${module}.load -o -e ${module}.conf",
    absent  => "! -e ${module}.load -a ! -e ${module}.conf",
  }

  # Enable or disable the module using a2enmod or a2dismod.
  exec { $cmd:
    command => $cmd,
    path    => '/usr/sbin:/usr/bin',
    require => Package['apache'],
    unless  => "[ $condition ]",
    notify  => Exec['apache reload'];
  }
}
