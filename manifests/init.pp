# Sets up an Apache server and installs WebAuth, since we almost never want an
# Apache server without WebAuth configuration.
#
# FIXME: In retrospect, this was a mistake; we need to make anyone using
# WebAuth explicitly include it so that servers that don't need it don't need
# overrides.

class apache (
  $include_webauth = true,
)
{
  if ($include_webauth) {
    include webauth
  }

  if ($osfamily == 'RedHat') {
    package { 'apache':
      name   => 'httpd',
      ensure => present,
    }
  } elsif ($osfamily == 'Debian') {
    if (($::lsbdistcodename == 'wheezy') or ($::lsbdistcodename == 'jessie')) {
      package { 'apache':
        name   => 'apache2-mpm-prefork',
        ensure => present,
      }
    } else {
      package { 'apache':
        name   => 'apache2',
        ensure => present,
      }
    }
  } else {
    crit("do not know what to do")
  }

  # Name the service apache, which doesn't match the name from either OS but
  # is easier to remember.
  service { 'apache':
    name        => $osfamily ? {
      'Debian' => 'apache2',
      'RedHat' => 'httpd',
    },
    ensure     => running,
    hasrestart => true,
    hasstatus  => $osfamily ? {
      'RedHat' => false,
      default  => true,
    },
    status     => $osfamily ? {
      'RedHat' => 'pidof httpd',
      default  => undef,
    },
    require    => Package['apache'],
  }

  # Force a reload after a configuration change.  The Debian init script
  # does a configtest by default, but the Red Hat one does not, so force Red
  # Hat to do the right thing.
  exec { 'apache reload':
    path       => '/bin:/usr/bin:/sbin:/usr/sbin',
    command    => $osfamily ? {
      'Debian' => 'service apache2 reload',
      'RedHat' => 'sh -c "apachectl configtest && /etc/init.d/httpd reload"',
    },
    refreshonly => true,
    require     => Package['apache'],
  }

  # Operating-system-specific Apache tweaks.
  case $osfamily {
    'Debian': { include apache::debian }
    'RedHat': { include apache::redhat }
  }

  # Default syslog filtering and iptables configuration.
  file { '/etc/filter-syslog/apache':
    source => 'puppet:///modules/apache/etc/filter-syslog/apache',
  }
  base::iptables::rule { 'web':
    description => 'Allow web accesses from anywhere',
    protocol    => 'tcp',
    port        => [ 80, 443 ],
  }
}
