release/002.001 (2018-06-21)

    Update conf.pp to handle stretch and enable Apache mod_ssl on
    every Apache server. (adamhl)

release/002.000 (2018-04-30)

    Change 'cert-files' to 'cert_files' (modules cannot have dashes!)
    (adamhl)

release/001.024 (2018-04-17)

    Update for stretch. (adamhl)

release/001.023 (2017-11-08)

    Add "apache::debian::disable_3DES" parameter to disable DES and
    3DES. Also reorganize class files a bit. (adamhl)

release/001.022 (2017-11-02)

    Add include_webauth parameter to give folks the option of not
    including webauth; defaults to true (so, no change to default
    behavior. (adamhl)

release/001.021 (2017-06-07)

    Change parameter from "apache::debian::disable_TLS10R4" to
    "apache::debian::disable_TLS10RC4". (adamhl).

release/001.020 (2017-06-06)

    Add apache::debian::disable_TLS10R4 parameter so that TLS1.0/RC4 can
    be disabled. Currently, that parameter defaults to false, so to
    disable TLS1.0/RC4 change it to true. Only affects debian
    systems. (adamhl).

release/001.019 (2017-01-18)

    In apache::local attempt to set permissions on files in
    /etc/apache2/conf.d only for Debian wheezy (/etc/apache2/conf.d is no
    longer included on Debian releases from jessie onwards) (adamhl).

release/001.018 (2016-08-01)

    Ignore more innocuous Cyrus SASL GSSAPI messages (adamhl).

release/001.017 (2016-03-28)

    In jessie, install new configs after installing jessie (akkornel).
    Update Exec paths for apache commands (akkornel).
    Fix install order for security config in Debian (akkornel).

release/001.016 (2016-01-28)

    Add new option in debian class to diable TLSv1.0 and RC4 (adamhl).

release/001.015 (2015-12-14)

    Changes to comodo.pp to support CentOS (jlent)

release/001.014 (2015-05-13)

    Changes to use osfamily fact to support CentOS (jlent)

release/001.013 (2015-03-31)

    Add support for apache 2.4 on Debian jessie systems. (whm)

release/001.012 (2015-02-18)

    Rename self-signed class to self_signed for puppet3. (darrenp1)

release/001.011 (2014-12-10)

    Template @ fix for local.sample.erb. (adamhl)

release/001.010 (2014-10-17)

    Fix for RHEL6 certs dir. (darrenp1)

release/001.009 (2014-10-15)

    Missed some conf files when disabling SSLv3 due to
    POODLE. (adamhl)

release/001.008 (2014-10-15)

    ssl-strength: disable SSLv3 due to POODLE. (adamhl)

release/001.007 (2014-09-30)

    Added two new intermediate certificates:

    cert 1:
      subject CN: InCommon RSA Server CA
      issuer CN:  USERTrust RSA Certification Authority

    cert 2:
      subject CN: USERTrust RSA Certification Authority
      issuer CN:  AddTrust External CA Root

   Also added a bundle file with the above two and the root certificate
   "AddTrust External CA Root:

release/001.006 (2014-08-28)

    Explicitly disable the cipher suites eNULL (no encryption) and aNULL
    (no authentication). (adamhl)

release/001.005 (2014-04-02)

    Fix the exec in apache::site that calls a2ensite to always call it if
    the symlink does not exist, rather than keying off of the existence of
    the site configuration file.  (rra)

release/001.004 (2014-03-26)

    ca-certificates in a recent update for RHEL6 now provides the directory
    handling that we were taking care of. This handling conflicted with the
    package update. Note that if you are upgrading the package you should move
    aside the current /etc/ssl/certs, let the package recreate it, then oneshot
    to repopulate the certs dir. (vdc)

    Removed files/mysql-ca files that do not belong in this module and rewrote
    the s_mysql service class in the IDG Puppet repository to not use
    them. (adamhl)

release/001.003 (2014-03-15)

    Replace comodo-entrust-2015.pem with the new version from Comodo that
    contains an intermediate certificate signed by a 2048-bit root CA.
    (rra)

    Use the service command rather than running init scripts directly when
    reloading Apache, in preparation for support for non-sysvinit init
    systems.  (rra)

    Since apache::cert::comodo no longer does anything Comodo-specific,
    replace apache::cert::other with a simple wrapper around ::comodo.
    Eventually, both classes should be renamed.  (rra)

    Stop hashing the comodo-incommon-addtrust-bundle-2020.pem file,
    although continue to install it.  Some other manifests use this file
    as a trust anchor, so we need to keep it, but it's not meaningful to
    hash it into the /etc/ssl/certs directory.  Hash symlinks only work
    for files containing only a single certificate, and both of the
    certificates in this file are already provided elsewhere.  (rra)

    Fix formatting, valid parameter checks, variable references, and file
    layout to match current Puppet coding standards.  (rra)

    Remove the unused httpd-init.patch file.  (rra)

release/001.002 (2013-09-23)

    Cleanup: remove the deprecated files/certs directory.  Certs are now
    retrieved from the local cert-files module.  (mgoll)

release/001.001 (2013-09-16)

    Add /modules to all Puppet file source URLs.  (rra)
